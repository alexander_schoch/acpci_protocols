rm(list = ls())
library(ggplot2)
library(extrafont)
library(scales)


source("../resolution.R")


data = data.frame(read.table(file="data.dat"))
volume = data[1]
cond = data[2]
temp = data[3]

vol_left = volume[1:19,1]
cond_left = cond[1:19,1]
vol_right = volume[22:41, 1]
cond_right = cond[22:41, 1]


fit_left = lm(as.matrix(vol_left) ~ as.matrix(cond_left))
fit_right = lm(as.matrix(vol_right) ~ as.matrix(cond_right))

i1 = 213.94
s1 = 84.95
i2 = 6.0886
s2 = 5.947

p = ggplot() + theme_bw()
p = p + geom_line(aes(y = cond_left, x = predict(fit_left)), color="red")
p = p + geom_line(aes(y = cond_right, x = predict(fit_right)))
p = p + geom_abline(intercept = i1/s1, slope = 1/s1)
p = p + geom_abline(intercept = i2/s2, slope = 1/s2)
p = p + geom_point(aes(x = volume, y = cond), size=1)
p = p + geom_point(aes(x = 9.5575, y = 2.6309), size=3, pch = 1)
p = p + xlab(expression("Volume V/mL"))
p = p + ylab(expression("Conductivity " * kappa * "/" * mu * "S cm"^"-1"))
p = p + theme(text = element_text(size = 16, family = "Cormorant Garamond Light"))
p = p + theme(axis.title = element_text(size = 16, family = "CMU Serif"))
p = p + theme(axis.text.x = element_text(color = "black"), axis.text.y = element_text(color = "black"))
ggsave(filename = "7.2.6_leitfaehigkeitstitration.png", dpi = res)
