#include <iostream>
#include <cmath>

using namespace std;


int main ()
{
    double min, max, step;
    cin >> min;
    cin >> max;
    cin >> step;
    for (double i = max; i >= min; i -= step)
    {
        if (i < 10)
        {
            if (floor (i) == i)
                cout << "0" << i << ".0" << endl;
            else
                cout << "0" << i << endl;
        }
        else
        {
            if (floor (i) == i)
                cout << i << ".0" << endl;
            else
                cout << i << endl;
        }
    }
    return 0;
}
