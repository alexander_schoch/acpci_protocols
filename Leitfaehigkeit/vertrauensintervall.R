rm(list=ls())

# 7.2.2: k = conductibility, W = Water, dW = deinonized water, tW = tap water, k_TW = k(T)W
kW = c(0.507, 0.553, 0.441)
tW = c(20, 19.3, 19.2)
kdW = c(8.561, 8.417, 8.544)
tdW = c(21.4, 21.4, 21.5)
ktW = c(300.5, 302.1, 302.0)
ttW = c(21.1, 20.9, 21.0)

k = kW
t = tW
alpha = 0.02
k_TW = k[1] * (1 + (alpha * (t - t[1])))


# 7.2.3:



# below: Universal script
tf = k_TW

m = sum(tf)/length(tf)
s = sd(tf)
sm = s/sqrt(length(tf))

# freiheitsgrad = 2

ts95 = 4.3

cm95 = ts95 * sm

print(data.frame(cm95, s, m))
