rm(list = ls())
library(ggplot2)
library(extrafont)

source("../resolution.R")

data = data.frame(read.table(file="table_complete.dat"))
fit = lm(as.matrix(data[1]) ~ as.matrix(data[2]))
regression = predict(fit)

p = ggplot(data = data, aes(x = data[1], y = data[2])) + geom_point(size = 0.4) + theme_bw()
p = p + xlab(expression("Temperature T/°C"))
p = p + ylab(expression("Conductivity " * kappa * "/mS cm"^"-1"))
p = p + theme(text = element_text(size = 16, family = "Cormorant Garamond Light"))
p = p + theme(axis.title = element_text(size = 16, family = "CMU Serif"))
p = p + theme(axis.text.x = element_text(color = "black"), axis.text.y = element_text(color = "black"))
p = p + geom_line(aes(regression))
ggsave(filename = "7.2.3_temperaturabhaengigkeit.png", dpi = res)
