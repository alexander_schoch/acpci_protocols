-Information for myself for R:
  - standard lines:
    - top:
      - rm(list = ls())
        pdf('ex5.pdf', family="Cormorant Garamond Light")
        library(extrafont)
    - bottom:
      - dev.off()
  - Start R in terminal:
    - R
    - library("extrafont")
  - End R in terminal:
    - q()
