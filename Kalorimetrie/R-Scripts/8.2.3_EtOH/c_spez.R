rm(list = ls())
library(ggplot2)
library(extrafont)

# Unsicherheitsabschätzung: Csp / mF = 0.042 ( = 1.15% )
# Korrekturfaktor: x3 (wegen Kalorimeterkalibration und anderen Fehlerquellen)
error = 132

#data = data.frame(read.table(file="data_sorted.dat"))
#data = data.frame(read.table(file="data_correct.dat"))
data = data.frame(read.table(file="data_correct_with_deleted_values.dat"))

fit = lm(as.matrix(data[2]) ~ I(as.matrix(data[1])) + I(as.matrix(data[1])^2) + I(as.matrix(data[1])^3))
reg = predict(fit)

r = 1 #factor = 29
circ_x = c(64.86, 95.86, 69.32)
circ_y = c(3498, 2520, 3426)

p = ggplot(data = data, aes(x = data[1], y = reg)) + geom_point(size = 0.3) + theme_bw()
for(i in 1:3)
{
  p = p + annotate("path", x = circ_x[i] + r*cos(seq(0, 2 * pi, length.out = 100)), y = circ_y[i] + r*29*sin(seq(0, 2 * pi, length.out = 100)))
}
p = p + geom_line()
p = p + geom_errorbar(aes(ymin = data[2] - error, ymax = data[2] + error), width = 1)
p = p + geom_point(data = data, aes(x = data[1], y = data[2]))
p = p + xlab(expression("Mass-% Ethanol"))
p = p + ylab(expression("specific heat capacity " * "c"["p"] * ""^"sp" * "/ J " * K^-1 * " " * kg^-1))
p = p + theme(text = element_text(size = 16, family = "Cormorant Garamond Light"))
p = p + theme(axis.title = element_text(size = 16, family = "CMU Serif"))
p = p + theme(axis.text.x = element_text(color = "black"), axis.text.y = element_text(color = "black"))
p = p + ylim(1400, 4650)
#ggsave("c_spez.pdf")
#ggsave("c_spez_corrected.pdf")
ggsave("c_spez_with_deleted_values.pdf")
