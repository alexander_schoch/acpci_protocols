water_i = [21.91513, 22.16492, 22.39321]
water_f = [19.73269, 20.03493, 19.94596]
ice_i = [22.09163, 22.46688, 21.97811]
ice_f = [13.50917, 13.88795, 13.21533]
m_ice = [12.24, 12.04, 12.17]
m_water = [12.87, 12.39, 12.19]
c = 4.182 #J K-1 g-1

for w in range(0,3):
    for i in range(0,3):
        dt2 = (ice_i[i] - ice_f[i])
        dt1 = (water_i[w] - water_f[w])
        zaehler = (water_f[w] * m_water[w] * dt2)
        nenner = (m_ice[i] * dt1)
        brack = ( zaehler / nenner - ice_f[i])
        print c * brack
