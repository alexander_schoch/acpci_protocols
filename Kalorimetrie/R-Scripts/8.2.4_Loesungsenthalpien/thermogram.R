rm(list = ls())
dataPath = "../../source/data/8.2.4_1.dat"
xLabel = "Time t/s"
yLabel = "Temperature T/°C"
source("../include.R")
ggsave("thermogram.pdf")
