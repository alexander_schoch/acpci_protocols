import math
import numpy as n

U = 9.88
R = 10.2

t = 120.0
M = 53.49
m = [0.5024, 0.5025, 0.5028]
dTs = [0.2683, 0.2695, 0.2741]
dTc = [2.1549, 2.1719, 2.1025]

m = n.asarray(m)
dTs = n.asarray(dTs)
dTc = n.asarray(dTc)


for i in range(0,3):
    zw1 = (U*U) * t / R
    zw2 = M / m[i]
    zw3 = dTs[i] / dTc[i]
    print zw1 * zw2 * zw3
