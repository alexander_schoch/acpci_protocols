void setup ()
{
    //Calibration: Water
    double R = 10.2, U = 9.88;
    double t[] = { 120, 125, 122 };
    double dT[] = { 2.26454, 2.25901, 2.26433 };
    double[] CpK = new double[3];
    double[] Cp = new double[3];
    for (int i = 0; i < t.length; i++)
    {
        Cp[i] = U * U * t[i] / (R * dT[i]);
        CpK[i] = Cp[i] - (0.1 * 4182);         //empty calorimeter
    }
    double sum = 0;
    for (int j = 0; j < CpK.length; j++)
    {
        sum += CpK[j];
        println (CpK[j]);
    }
    sum /= CpK.length;

    println (sum);




    //ethanol:
/*
 *  double CpK = 99.23270408553412;
 *
 *  double[] t = { 127, 124, 123 };
 *  double[] dT = { 2.90642, 3.95933, 2.99364 };
 *  double[] mF = { 87.4019, 79.64, 85.89 };
 *
 *  double[] Cp = new double[3];
 *  double[] CpF = new double[3];
 *  for (int i = 0; i < t.length; i++)
 *  {
 *      Cp[i] = U * U * t[i] / (R * dT[i]);
 *      CpF[i] = (Cp[i] - CpK) / mF[i];
 *      println (CpF[i]);
 *  }
 */
    exit ();
}
